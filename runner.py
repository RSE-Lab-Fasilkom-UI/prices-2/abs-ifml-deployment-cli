#!/usr/bin/python3

import os
from argparse import ArgumentParser
from configparser import ConfigParser
from collections.abc import Mapping
from deployment import (deploy, start, refresh_nginx, stop, stop_inactive,
                        update, destroy)
from utils.shell import str_bool

__parser = ArgumentParser(
    description="ABS and IFML deployment and management tools.")


class Configuration(Mapping):
    __config = ConfigParser()

    def __init__(self, config_path="config.ini"):
        self.__config.read(config_path)

    def __getitem__(self, key):
        splitted_keys = key.split("::")
        if len(splitted_keys) == 2:
            return self.__config[splitted_keys[0]][splitted_keys[1]]
        elif len(splitted_keys) == 1:
            return self.__config[key]
        else:
            return IndexError("Only 2 levels of index are allowed.")

    def __iter__(self):
        for group in self.__config.__iter__():
            for key in self.__config[group].__iter__():
                yield "%s::%s" % (group, key)

    def __len__(self):
        return sum([len(self.__config[x]) for x in self.__config])


# Initialize parser
def __init_parser():
    subparsers = __parser.add_subparsers(title="subcommands",
                                         description="Valid subcommands",
                                         help="Sub-command help",
                                         dest="subcommand")
    __parser.add_argument("-cfg",
                          "--config-file",
                          default="~/abs_ifml_config.ini",
                          help="Configuration file location " +
                          "(default: [your_home_folder]/abs_ifml_config.ini)",
                          dest="config_file")

    __init_parser_deploy(subparsers)
    __init_parser_start(subparsers)
    __init_parser_refresh_nginx(subparsers)
    __init_parser_restart(subparsers)
    __init_parser_stop(subparsers)
    __init_parser_stop_inactive(subparsers)
    __init_parser_update(subparsers)
    __init_parser_destroy(subparsers)


def __init_parser_deploy(subparsers):
    parser_deploy = subparsers.add_parser('deploy',
                                          help='Start deployment right away.')
    parser_deploy.add_argument("product_name", help="product name (mandatory)")
    parser_deploy.add_argument("products_file",
                               help="Products.abs file location (mandatory)")
    parser_deploy.add_argument(
        "user_data_file", help="JSON file representing user data (mandatory)")
    parser_deploy.add_argument(
        "-mnt",
        "--maintenance-mode",
        action="store_true",
        help="after deployment, run the product in maintenance mode",
        dest="maintenance_mode")
    parser_deploy.add_argument("-prs",
                               "--preserve-source",
                               action="store_true",
                               help="preserve source code after deployment",
                               dest="preserve_source")
    parser_deploy.add_argument("-sub",
                               "--subdomain",
                               default="",
                               help="product subdomain (mandatory)",
                               dest="product_subdomain")
    parser_deploy.add_argument("-fe",
                               "--front-end",
                               default="angular",
                               choices=["angular", "react"],
                               help="choose frontend engines: React or Angular.",
                               dest="frontend_engine")
    parser_deploy.add_argument("-be",
                               "--back-end",
                               default="absjava",
                               choices=["absjava", "abserlang", "vmj"],
                               help="choose backend engines: ABS Java, ABS Erlang, or VMJ.",
                               dest="backend_engine")
    parser_deploy.add_argument("-ssl",
                               "--enable-ssl",
                               action="store_true",
                               help="enable access via HTTPS",
                               dest="enable_ssl")
    parser_deploy.add_argument("--roles",
                               default="{}",
                               help='JSON list of roles and emails' +
                               ' (will be ignored if -rf is defined), ' +
                               'example: {"admin": ["(email-1)", ...], ...}',
                               dest="roles")
    parser_deploy.add_argument("--roles-file",
                               help="roles JSON file path",
                               dest="roles_file_path")
    parser_deploy.add_argument(
        "-auth",
        "--auth-type",
        help="OAuth2 authentication method",
        default="disabled",
        choices=["disabled", "auth0", "google", "facebook"],
        dest="oauth2_type")
    parser_deploy.add_argument("--auth-id",
                               default="",
                               help="OAuth2 Client ID (must be defined if" +
                               " -auth is not disabled) [if -auth is 'auth0'" +
                               ", this must be filled with your Auth0 domain]",
                               dest="oauth2_client_id")
    parser_deploy.add_argument("--auth-secret",
                               default="",
                               help="OAuth2 Client Secret (some OAuth" +
                               " methods require it, e.g. facebook) [if" +
                               " -auth is 'auth0', this must be filled with" +
                               " your Auth0 Client ID]",
                               dest="oauth2_client_secret")
    parser_deploy.add_argument("--docker",
                                type=str_bool,
                                default=False,
                                help="Write 'True' or 't' if want to user Docker. Write 'False' or 'f' to not use Docker",
                                dest="docker_deploy")


def __init_parser_start(subparsers):
    parser_start = subparsers.add_parser(
        'start', help='Run instances of a deployed product.')
    parser_start.add_argument("product_name", help="Product name (mandatory)")
    parser_start.add_argument("--docker",
                                type=str_bool,
                                default=False,
                                help="Write 'True' or 't' if want to user Docker. Write 'False' or 'f' to not use Docker",
                                dest="docker_deploy")


def __init_parser_refresh_nginx(subparsers):
    parser_refresh = subparsers.add_parser(
        'refresh_nginx',
        help='Refresh NGINX (enable/disable maintenance mode).')
    parser_refresh.add_argument("product_name",
                                help="Product name (mandatory)")
    parser_refresh.add_argument("-mnt",
                                "--maintenance-mode",
                                action="store_true",
                                help="enable maintenance mode",
                                dest="maintenance_mode")
    parser_refresh.add_argument("--docker",
                                type=str_bool,
                                default=False,
                                help="Write 'True' or 't' if want to user Docker. Write 'False' or 'f' to not use Docker",
                                dest="docker_deploy")


def __init_parser_restart(subparsers):
    parser_stop = subparsers.add_parser(
        'restart',
        help='Restart partially/fully running instances of a deployed product.'
    )
    parser_stop.add_argument("product_name", help="Product name (mandatory)")


def __init_parser_stop(subparsers):
    parser_stop = subparsers.add_parser(
        'stop', help='Temporarily terminate instances of a deployed product.')
    parser_stop.add_argument("product_name", help="Product name (mandatory)")
    parser_stop.add_argument("--docker",
                                type=str_bool,
                                default=False,
                                help="Write 'True' or 't' if want to user Docker. Write 'False' or 'f' to not use Docker",
                                dest="docker_deploy")


def __init_parser_stop_inactive(subparsers):
    parser_stop_inactive = subparsers.add_parser(
        'stop_inactive',
        help='Stop products that are inactive for a defined time interval.')
    parser_stop_inactive.add_argument(
        "interval",
        help="Maximum last access time interval from current time." +
        " Format: <digit><unit>. Only supports single time unit." +
        " Units: w (week), d (day), h (hour), m (minute), s (second).")


def __init_parser_update(subparsers):
    parser_update = subparsers.add_parser(
        'update', help='Update a deployed product using latest sources.')
    parser_update.add_argument("product_name",
                                help="Product name (mandatory)")
    parser_update.add_argument("-f",
                                "--frontend",
                                action="store_true",
                                help="updates the frontend",
                                dest="frontend")
    parser_update.add_argument("-b",
                                "--backend",
                                action="store_true",
                                help="updates the backend",
                                dest="backend")
    parser_update.add_argument("-a",
                                "--admin",
                                action="store_true",
                                help="updates the admin API",
                                dest="admin")
    parser_update.add_argument("-prs",
                               "--preserve-source",
                               action="store_true",
                               help="preserve source code after deployment",
                               dest="preserve_source")


def __init_parser_destroy(subparsers):
    parser_destroy = subparsers.add_parser('destroy',
                                           help='Delete a deployed product.')
    parser_destroy.add_argument("product_name",
                                help="Product name (mandatory)")
    parser_destroy.add_argument("--docker",
                                type=str_bool,
                                default=False,
                                help="Write 'True' or 't' if want to user Docker. Write 'False' or 'f' to not use Docker",
                                dest="docker_deploy")


# Main function
def __main__():
    args = __parser.parse_args()
    config = Configuration(
        config_path=os.path.abspath(os.path.expanduser(args.config_file)))

    if args.subcommand == "deploy":
        return deploy.run_command(args, config)
    if args.subcommand == "start":
        return start.run_command(args, config)
    if args.subcommand == "refresh_nginx":
        return refresh_nginx.run_command(args, config)
    if args.subcommand == "restart":
        stop.run_command(args, config)
        return start.run_command(args, config)
    if args.subcommand == "stop":
        return stop.run_command(args, config)
    if args.subcommand == "stop_inactive":
        return stop_inactive.run_command(args, config)
    if args.subcommand == "update":
        return update.run_command(args, config)
    if args.subcommand == "destroy":
        return destroy.run_command(args, config)
    raise IllegalArgumentError("Invalid subcommand.")


if __name__ == '__main__':
    __init_parser()
    __main__()


class IllegalArgumentError(ValueError):
    pass
