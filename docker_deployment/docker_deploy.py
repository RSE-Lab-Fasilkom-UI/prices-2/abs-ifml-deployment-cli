import docker
import os
import json
import logging
import requests
import subprocess
import shutil
from threading import Thread
from utils.administration import APP_ROOT_PATH
from utils.template import fill_template

client = docker.from_env()

REPOSITORY_DOCKER = "test-dio.tk"
SERVER_DEPLOY_URL = "http://flask.claudioyosafat.live/"
NGINX_SERVER_DEPLOY_CONF = "_nginx_server"

def deploy_image(product_name, product_path, nginx_port, frontend_engine, maintenance_mode):
    try:
        logging.basicConfig(filename = "docker_deployment/" + product_name + '.log', filemode = 'w', format = '%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s', level=logging.DEBUG)

        cli = docker.APIClient(base_url='unix://var/run/docker.sock')
        logging.info('===Building docker image of product name %s===', product_name)
        builder = cli.build(path = product_path, tag = product_name, decode = True)

        for data in builder:
            if 'stream' in data:
                for line in data['stream'].splitlines():
                    logging.debug(line)
            elif 'error' in data:
                logging.error(data['error'])
                return False

        #tagging image
        repository_with_image = REPOSITORY_DOCKER + "/" + product_name
        image = client.images.get(product_name)
        image.tag(repository_with_image)

        #push image to private registries
        uploader = client.images.push(repository_with_image, stream = True, decode = True)

        for data in uploader:
            logging.debug(data)

        post_data = {'product_name': product_name, 'nginx_port': nginx_port, 'repository': REPOSITORY_DOCKER, 'frontend_engine': frontend_engine, 'maintenance_mode': maintenance_mode}

        requests.post(SERVER_DEPLOY_URL + "dockerdeploy", json = post_data)

        client.images.remove(product_name)
        client.images.remove(REPOSITORY_DOCKER + "/" + product_name)
    except Exception as e:
        return False
    return True

def setup_deploy_server(payload, product_name, backend_product_path, nginx_product_path):
    if not os.path.exists(NGINX_SERVER_DEPLOY_CONF):
        os.makedirs(NGINX_SERVER_DEPLOY_CONF)

    files = dict()
    temp_payload_maintenance = payload['maintenance_mode']

    with open('data.txt', 'w') as outfile:
        json.dump({'maintenance_mode': payload['maintenance_mode']}, outfile)
    files['maintenance_mode'] = open('data.txt', 'rb')
    os.remove('data.txt')

    payload['product_name'] = payload['product_name'].lower()

    SOURCE_PATH = os.path.join(APP_ROOT_PATH, "templates")

    nginx_conf_mnt = os.path.join(
                NGINX_SERVER_DEPLOY_CONF, product_name + "MNT.conf")      
    shutil.copy(os.path.join(
        SOURCE_PATH, "nginx.server.template.conf"), nginx_conf_mnt)
    nginx_conf = os.path.join(
            NGINX_SERVER_DEPLOY_CONF, product_name + ".conf")     
    shutil.copy(os.path.join(
        SOURCE_PATH, "nginx.server.template.conf"), nginx_conf)

    if temp_payload_maintenance:
        fill_template(nginx_conf_mnt, payload)
        payload['maintenance_mode'] = False
        fill_template(nginx_conf, payload)
        
    else:
        fill_template(nginx_conf, payload)
        payload['maintenance_mode'] = True
        fill_template(nginx_conf_mnt, payload)
        

    payload['maintenance_mode'] = temp_payload_maintenance
    
    files['nginx_conf'] = open(os.path.join(nginx_conf), 'rb')
    files['nginx_conf_mnt'] = open(os.path.join(nginx_conf_mnt), 'rb')
    files['auth'] = open(os.path.join(backend_product_path, 'auth.properties'), 'rb')
    files['config'] = open(os.path.join(backend_product_path, 'config.properties'), 'rb')
    files['404_page'] = open(os.path.join(nginx_product_path, '404.html'), 'rb')
    files['maintenance_page'] = open(os.path.join(nginx_product_path, 'maintenance.html'), 'rb')
    result = requests.post(SERVER_DEPLOY_URL + "setupserver", files = files)
    return True

def get_deploy_port():
    nginx_port = requests.post(SERVER_DEPLOY_URL + "nginxport")
    return nginx_port.json()['port']

def docker_stop(product_name):
    post_data = {'product_name': product_name}

    return requests.post(SERVER_DEPLOY_URL + "dockerstop", json = post_data)

def docker_start(product_name):
    post_data = {'product_name': product_name}

    return requests.post(SERVER_DEPLOY_URL + "dockerstart", json = post_data)

def docker_destroy(product_name):
    post_data = {'product_name': product_name}

    return requests.post(SERVER_DEPLOY_URL + "dockerdestroy", json = post_data)

def docker_refresh_nginx(product_name, maintenance_mode):
    post_data = {'product_name' : product_name, 'maintenance_mode' : maintenance_mode , 'server': '1'}

    return requests.post(SERVER_DEPLOY_URL + "refreshnginx", json = post_data)
