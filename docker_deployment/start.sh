#!/bin/bash

nginx

cd /frontend/
. run.sh &
cd ..

cd /backend/
. run.sh &
cd ..

cd /admin/
python3 gev.py 
cd ..