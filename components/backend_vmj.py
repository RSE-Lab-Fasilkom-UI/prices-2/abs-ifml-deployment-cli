import json
import os
import shutil
import traceback
from distutils import dir_util
from components.backend import BackendInstance
from utils.administration import APP_ROOT_PATH
from utils.db import query_to_database
from utils.ports import reserve_port, release_port
from utils.shell import (get_script_extension, read_pid_file,
                         write_pid_to_file, run_process, stop_process)
from utils.template import fill_template


class BackendVMJInstance(BackendInstance):
    def __init__(self, payload, config):
        super().__init__(payload, config)
        self.SOURCE_PATH = os.path.abspath(
            os.path.expanduser(config["source_paths::backend_vmj"]))
        self.product_qualifier = self.config['general::generator_codename'].lower() \
            + ".product." + self.payload["product_name"].lower()
        self.product_name = self.payload["product_name"]

    def build(self, preserve_source=False):
        try:
            if not os.path.exists(os.path.dirname(self.DEPLOY_LOG_PATH)):
                os.makedirs(os.path.dirname(self.DEPLOY_LOG_PATH),
                            exist_ok=True)
            shutil.copytree(self.SOURCE_PATH,
                            self.get_destination(),
                            ignore=shutil.ignore_patterns(".git"))
            self.__apply_templates()
            self.__fix_script_permissions()
            self.__build_vmj()
        except Exception:
            traceback.print_exc()
            return False
        return True

    def is_running(self):
        return read_pid_file(self.get_destination())

    def run(self, socket):
        release_port(socket)
        existing_pid = read_pid_file(self.get_destination())
        if not existing_pid:
            with open(self.RUN_LOG_PATH, "a") as f:
                cmd = [
                    os.path.join(self.get_destination(),
                                 "run" + get_script_extension()),
                    self.product_qualifier
                ]
                proc = run_process(cmd, log=f, cwd=self.get_destination())
                write_pid_to_file(proc, self.get_destination())

    def stop(self):
        pid = read_pid_file(self.get_destination())
        if pid:
            stop_process(pid)
            write_pid_to_file(None, self.get_destination())

    def update(self, preserve_source=False):
        self.set_destination(self.UPDATE_TEMP_PATH)
        shutil.rmtree(self.UPDATE_TEMP_PATH, ignore_errors=True)
        result = self.build(preserve_source=preserve_source)

        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["backend"])

        shutil.rmtree(self.OLD_INSTANCE_PATH, ignore_errors=True)
        shutil.move(self.CURRENT_INSTANCE_PATH, self.OLD_INSTANCE_PATH)
        shutil.move(self.get_destination(), self.CURRENT_INSTANCE_PATH)
        self.set_destination(self.CURRENT_INSTANCE_PATH)
        self.run(reserved_port)
        return result

    def revert(self):
        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["backend"])

        dir_util.remove_tree(self.get_destination())
        shutil.move(self.OLD_INSTANCE_PATH, self.get_destination())
        self.run(reserved_port)

    def destroy(self):
        shutil.rmtree(self.get_destination(), ignore_errors=True)

    def __apply_templates(self, **kwargs):
        variables = dict(self.payload)
        variables.update(kwargs)
        with open(os.path.join(self.get_destination(), "feature_mapping.json"), "r") as mapping_json:
            variables.update({"vmj_mapping": json.loads(mapping_json.read())})
        template_root = os.path.join(self.get_destination(), "src", "product.template")
        template_destinations = {
            "module-info.template.java": ["src", self.product_qualifier, "module-info.java"],
            "Product.template.java": ["src", self.product_qualifier, *self.product_qualifier.split("."), self.product_name + ".java"],
            "auth.template.properties": ["auth.properties"]
        }

        for file_name, dest_rel_path in template_destinations.items():
            file_path = os.path.join(template_root, file_name)
            dest_path = os.path.join(self.get_destination(), *dest_rel_path)
            fill_template(file_path, variables)
            if not os.path.exists(os.path.dirname(dest_path)):
                os.makedirs(os.path.dirname(dest_path), exist_ok=True)
            shutil.copy(file_path, dest_path)
        dir_util.remove_tree(template_root)

    def __fix_script_permissions(self):
        os.chmod(
            os.path.join(self.get_destination(), "genproduct" + get_script_extension()),
            0o775)
        os.chmod(
            os.path.join(self.get_destination(), "run" + get_script_extension()),
            0o775)

    def __build_vmj(self):
        with open(self.DEPLOY_LOG_PATH, "a") as f:
            cmd = [
                os.path.join(self.get_destination(),
                             "genproduct" + get_script_extension()),
                self.product_qualifier,
                self.product_name
            ]
            vmj_returncode = run_process(cmd, wait=True, log=f)
        if vmj_returncode != 0:
            raise IOError("VMJ build failed with error code: %s" %
                          (vmj_returncode, ))
