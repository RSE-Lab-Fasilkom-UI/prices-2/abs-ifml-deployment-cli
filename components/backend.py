import os
import shutil
from utils.administration import get_product_root


class BackendInstance():
    def __init__(self, payload, config):
        self.config = config
        self.payload = dict(payload)
        self.payload.update(
            {"db_" + k: x
             for k, x in self.config["database"].items()})

        self.PRODUCT_ROOT = get_product_root(config, payload["product_name"])
        self.DEPLOY_LOG_PATH = os.path.join(
            self.PRODUCT_ROOT, self.config["logs::backend_deploy"])
        self.RUN_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                         self.config["logs::backend_run"])
        self.UPDATE_TEMP_PATH = os.path.join(self.PRODUCT_ROOT,
                                             "backend_update", "")
        self.OLD_INSTANCE_PATH = os.path.join(self.PRODUCT_ROOT, "backend_old",
                                              "")
        self.CURRENT_INSTANCE_PATH = os.path.join(self.PRODUCT_ROOT, "backend", "")
        self.__destination = self.CURRENT_INSTANCE_PATH

    def set_destination(self, destination):
        self.__destination = destination

    def get_destination(self):
        return self.__destination

    def build(self, preserve_source=False):
        raise NotImplementedError

    def run(self, socket):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError

    def update(self, preserve_source=False):
        raise NotImplementedError

    def revert(self):
        raise NotImplementedError

    def destroy(self):
        shutil.rmtree(self.get_destination(), ignore_errors=True)
