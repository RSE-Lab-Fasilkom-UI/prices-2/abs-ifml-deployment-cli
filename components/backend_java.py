import os
import shutil
import traceback
from distutils import dir_util
from components.backend import BackendInstance
from utils.administration import APP_ROOT_PATH
from utils.db import query_to_database
from utils.ports import reserve_port, release_port
from utils.shell import (get_script_extension, read_pid_file,
                         write_pid_to_file, run_process, stop_process)
from utils.template import fill_template


class BackendJavaInstance(BackendInstance):
    def __init__(self, payload, config):
        super().__init__(payload, config)
        self.SOURCE_PATH = os.path.abspath(
            os.path.expanduser(config["source_paths::backend_absjava"]))
        self.template_root = os.path.join(self.get_destination(), "template")
        self.template_paths = [
            ["auth.properties"],
            ["build.xml"],
            ["build.bat"],
            ["build.sh"],
            ["config.properties"],
            ["config_ori.properties"],
            ["run.bat"],
            ["run.sh"],
            ["src", "abs", "framework", "Products.abs"],
            ["src", "java", "com", "rse", "absserver", "ABSServer.java"],
            ["src", "sql", "ChartOfAccountImpl.sql"],
        ]

    def build(self, preserve_source=False):
        try:
            if not os.path.exists(os.path.dirname(self.DEPLOY_LOG_PATH)):
                os.makedirs(os.path.dirname(self.DEPLOY_LOG_PATH),
                            exist_ok=True)
            shutil.copytree(self.SOURCE_PATH,
                            self.get_destination(),
                            ignore=shutil.ignore_patterns(".git"))

            self.__apply_templates(preserve_source=preserve_source)
            self.__fix_script_permissions()
            self.__build_abs_to_java()
            self.__create_database_user()
        except Exception:
            traceback.print_exc()
            return False
        return True

    def is_running(self):
        return read_pid_file(self.get_destination())

    def run(self, socket):
        release_port(socket)
        existing_pid = read_pid_file(self.get_destination())
        if not existing_pid:
            with open(self.RUN_LOG_PATH, "a") as f:
                cmd = [
                    os.path.join(self.get_destination(),
                                 "run" + get_script_extension())
                ]
                proc = run_process(cmd, log=f, cwd=self.get_destination())
                write_pid_to_file(proc, self.get_destination())

    def stop(self):
        pid = read_pid_file(self.get_destination())
        if pid:
            stop_process(pid)
            write_pid_to_file(None, self.get_destination())

    def update(self, preserve_source=False):
        self.set_destination(self.UPDATE_TEMP_PATH)
        shutil.rmtree(self.UPDATE_TEMP_PATH, ignore_errors=True)
        result = self.build(preserve_source=preserve_source)

        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["backend"])

        shutil.rmtree(self.OLD_INSTANCE_PATH, ignore_errors=True)
        shutil.move(self.CURRENT_INSTANCE_PATH, self.OLD_INSTANCE_PATH)
        shutil.move(self.get_destination(), self.CURRENT_INSTANCE_PATH)
        self.set_destination(self.CURRENT_INSTANCE_PATH)
        self.run(reserved_port)
        return result

    def revert(self):
        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["backend"])

        dir_util.remove_tree(self.get_destination())
        shutil.move(self.OLD_INSTANCE_PATH, self.get_destination())
        self.run(reserved_port)

    def destroy(self):
        shutil.rmtree(self.get_destination(), ignore_errors=True)
        self.__drop_database_and_user()

    def __apply_templates(self, **kwargs):
        variables = dict(self.payload)
        variables.update(kwargs)
        for file_path in self.template_paths:
            fill_template(os.path.join(self.template_root, *file_path),
                          variables)
        dir_util.copy_tree(self.template_root, self.get_destination())
        dir_util.remove_tree(self.template_root)

    def __fix_script_permissions(self):
        os.chmod(
            os.path.join(self.get_destination(), "build" + get_script_extension()),
            0o775)
        os.chmod(
            os.path.join(self.get_destination(), "run" + get_script_extension()),
            0o775)

    def __build_abs_to_java(self):
        with open(self.DEPLOY_LOG_PATH, "a") as f:
            cmd = [
                os.path.join(self.get_destination(),
                             "build" + get_script_extension())
            ]
            abs_returncode = run_process(cmd, wait=True, log=f)
        if abs_returncode != 0:
            raise IOError("ABS build failed with error code: %s" %
                          (abs_returncode, ))

    def __create_database_user(self):
        user_query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                                "create_user.template.sql"),
                                   self.payload,
                                   write_to_file=False)
        query_to_database(user_query,
                          self.config,
                          database_name=self.payload["product_subdomain"])
        shutil.move(os.path.join(self.get_destination(), "config_ori.properties"),
                    os.path.join(self.get_destination(), "config.properties"))

    def __drop_database_and_user(self):
        user_query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                                "drop_database.template.sql"),
                                   self.payload,
                                   write_to_file=False)
        query_to_database(user_query, self.config)
