import os
import shutil
import traceback
from distutils import dir_util
from utils.administration import get_product_root
from utils.ports import reserve_port, release_port
from utils.shell import (read_pid_file, write_pid_to_file, run_process,
                         stop_process, get_python_exec)


class AdminInstance():
    def __init__(self, payload, config):
        self.config = config
        self.payload = payload
        self.SOURCE_PATH = os.path.abspath(
            os.path.expanduser(self.config["source_paths::admin"]))
        self.PRODUCT_ROOT = get_product_root(config, payload["product_name"])
        self.DEPLOY_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                            self.config["logs::admin_deploy"])
        self.RUN_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                         self.config["logs::admin_run"])
        self.UPDATE_TEMP_PATH = os.path.join(self.PRODUCT_ROOT, "admin_update",
                                             "")
        self.OLD_INSTANCE_PATH = os.path.join(self.PRODUCT_ROOT, "admin_old",
                                              "")
        self.CURRENT_INSTANCE_PATH = os.path.join(self.PRODUCT_ROOT, "admin", "")
        self.destination = self.CURRENT_INSTANCE_PATH

    def set_destination(self, destination):
        self.destination = destination

    def build(self):
        try:
            if not os.path.exists(os.path.dirname(self.DEPLOY_LOG_PATH)):
                os.makedirs(os.path.dirname(self.DEPLOY_LOG_PATH),
                            exist_ok=True)
            shutil.copytree(self.SOURCE_PATH,
                            self.destination,
                            ignore=shutil.ignore_patterns(".git"))
            os.symlink(
                os.path.join(self.PRODUCT_ROOT,
                             self.payload["product_name"] + ".json"),
                os.path.join(self.destination, "properties.json"))
            os.symlink(self.RUN_LOG_PATH,
                       os.path.join(self.destination, "flask.log"))
            self.__fix_script_permissions()

        except Exception:
            traceback.print_exc()
            return False
        return True

    def is_running(self):
        return read_pid_file(self.destination)

    def run(self, socket):
        release_port(socket)
        existing_pid = read_pid_file(self.destination)
        if not existing_pid:
            with open(self.RUN_LOG_PATH, "a") as f:
                cmd = [
                    get_python_exec(),
                    os.path.join(self.destination, "gev.py")
                ]
                run_process(cmd, log=f, cwd=self.destination)

    def stop(self):
        pid = read_pid_file(self.destination)
        if pid:
            stop_process(pid)
            write_pid_to_file(None, self.destination)

    def update(self):
        self.set_destination(self.UPDATE_TEMP_PATH)
        shutil.rmtree(self.UPDATE_TEMP_PATH, ignore_errors=True)
        result = self.build()

        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["admin"])

        current_static = os.path.join(self.CURRENT_INSTANCE_PATH, "static")
        current_upload = os.path.join(self.CURRENT_INSTANCE_PATH, "upload")
        temp_static = os.path.join(self.PRODUCT_ROOT, "static_temp")
        temp_upload = os.path.join(self.PRODUCT_ROOT, "upload_temp")
        shutil.move(current_static, temp_static)
        shutil.move(current_upload, temp_upload)

        shutil.rmtree(self.OLD_INSTANCE_PATH, ignore_errors=True)
        shutil.move(self.CURRENT_INSTANCE_PATH, self.OLD_INSTANCE_PATH)
        shutil.move(self.destination, self.CURRENT_INSTANCE_PATH)
        shutil.move(temp_static, current_static)
        shutil.move(temp_upload, current_upload)

        self.set_destination(self.CURRENT_INSTANCE_PATH)
        self.run(reserved_port)
        return result

    def revert(self):
        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["admin"])

        current_static = os.path.join(self.destination, "static")
        current_upload = os.path.join(self.destination, "upload")
        temp_static = os.path.join(self.PRODUCT_ROOT, "static_temp")
        temp_upload = os.path.join(self.PRODUCT_ROOT, "upload_temp")
        shutil.move(current_static, temp_static)
        shutil.move(current_upload, temp_upload)

        dir_util.remove_tree(self.destination)
        shutil.move(self.OLD_INSTANCE_PATH, self.destination)
        shutil.move(temp_static, current_static)
        shutil.move(temp_upload, current_upload)
        self.run(reserved_port)

    def destroy(self):
        shutil.rmtree(self.destination, ignore_errors=True)

    def __fix_script_permissions(self):
        os.chmod(os.path.join(self.destination, "app.py"), 0o775)
        os.chmod(os.path.join(self.destination, "gev.py"), 0o775)
