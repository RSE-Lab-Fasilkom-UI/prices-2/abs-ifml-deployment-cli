import json
import os
import re
import shutil
import traceback
from distutils import dir_util
from pathlib import Path
from components.frontend import FrontendInstance
from utils.administration import (read_json_payload, write_json_payload,
                                  write_abs_products_file, APP_ROOT_PATH,
                                  normalize_name_dash)
from utils.ports import reserve_port, release_port
from utils.shell import (get_script_extension, read_pid_file,
                         write_pid_to_file, run_process, stop_process)


class FrontendReactInstance(FrontendInstance):
    def __init__(self, payload, config):
        super().__init__(payload, config)
        self.SOURCE_PATH = os.path.abspath(
            os.path.expanduser(config["source_paths::frontend_react"]))
        self.TEMPLATE_PATH =  os.path.join(APP_ROOT_PATH, "templates")
        self.temp_folder = os.path.join(self.PRODUCT_ROOT, "frontend_temp", "")
        self.json_path = os.path.join(self.PRODUCT_ROOT,
                                      self.payload["product_name"] + ".json")
        self.abs_products_path = os.path.join(self.PRODUCT_ROOT, "Products.abs")
        self.product_init_files_path = os.path.join(self.temp_folder, "src")
        self.dist_path = os.path.join(self.temp_folder, "src-gen", "result")

    def build(self, preserve_source=False):
        try:
            if not os.path.exists(os.path.dirname(self.DEPLOY_LOG_PATH)):
                os.makedirs(os.path.dirname(self.DEPLOY_LOG_PATH),
                            exist_ok=True)
            shutil.copytree(self.SOURCE_PATH,
                            self.temp_folder,
                            ignore=shutil.ignore_patterns(".git"))
            if not os.path.exists(self.dist_path):
                os.makedirs(self.dist_path)

            self.__update_product_json()
            self.__symlink_product_files()
            self.__generate_pwa_from_ifml()

            dir_util.copy_tree(
                os.path.join(self.dist_path, normalize_name_dash(
                    self.payload["original_product_name"])),
                self.get_destination())
            dir_util.remove_tree(self.temp_folder)

            self.__create_node_modules_symlink()
            self.__build_pwa()

            if not preserve_source:
                self.__clean_source_code()
        except Exception:
            traceback.print_exc()
            return False
        return True

    def is_running(self):
        return read_pid_file(self.get_destination())

    def run(self, socket):
        release_port(socket)

    def stop(self):
        pid = read_pid_file(self.get_destination())
        if pid:
            stop_process(pid)
            write_pid_to_file(None, self.get_destination())

    def update(self, preserve_source=False):
        self.set_destination(self.UPDATE_TEMP_PATH)
        shutil.rmtree(self.UPDATE_TEMP_PATH, ignore_errors=True)
        result = self.build(preserve_source=preserve_source)

        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["frontend"])
        shutil.rmtree(self.OLD_INSTANCE_PATH, ignore_errors=True)
        shutil.move(self.CURRENT_INSTANCE_PATH, self.OLD_INSTANCE_PATH)
        shutil.move(self.get_destination(), self.CURRENT_INSTANCE_PATH)
        self.set_destination(self.CURRENT_INSTANCE_PATH)
        self.run(reserved_port)
        return result

    def revert(self):
        self.stop()
        reserved_port = reserve_port(self.payload["ports"]["frontend"])

        dir_util.remove_tree(self.get_destination())
        shutil.move(self.OLD_INSTANCE_PATH, self.get_destination())
        self.run(reserved_port)

    def destroy(self):
        shutil.rmtree(self.get_destination(), ignore_errors=True)

    def __update_product_json(self):
        if not self.payload.get("interface_variation"):
            variation_path = os.path.join(self.TEMPLATE_PATH,
                                          "react_interface_variation.template.json")
            with open(variation_path, "r") as f:
                interface_variation = json.loads(f.read())
            self.payload.update(interface_variation)
            write_json_payload(self.payload, self.config)

    def __symlink_product_files(self):
        write_abs_products_file(self.payload, self.config)
        os.symlink(self.json_path,
                   os.path.join(self.product_init_files_path, "products",
                                self.payload["product_name"] + ".json"))
        os.symlink(self.abs_products_path,
                   os.path.join(self.product_init_files_path, "products",
                                "Products.abs"))

    def __generate_pwa_from_ifml(self):
        with open(self.DEPLOY_LOG_PATH, "a") as f:
            ifml_cmd = [
                os.path.join(self.temp_folder, "generate" + get_script_extension()),
                self.payload["product_name"],
                normalize_name_dash(self.payload["original_product_name"])
            ]
            ifml_returncode = run_process(ifml_cmd,
                                          wait=True,
                                          log=f,
                                          cwd=self.temp_folder)
        if ifml_returncode != 0:
            raise IOError("IFML build failed with error code: %s" %
                          (ifml_returncode, ))

    def __create_node_modules_symlink(self):
        os.makedirs(os.path.join(self.NODE_SHARED_PATH, "node_modules"),
                    exist_ok=True)
        Path(os.path.join(self.NODE_SHARED_PATH, "yarn.lock")).touch()
        if self.config.get("work_paths::node_location"):
            os.symlink(os.path.join(self.NODE_SHARED_PATH, "node_modules"),
                       os.path.join(self.get_destination(), "node_modules"),
                       target_is_directory=True)

    def __build_pwa(self):
        self.__install_dependencies()
        self.__run_build_pwa()

    def __install_dependencies(self):
        with open(self.DEPLOY_LOG_PATH, "a") as f:
            pwa_cmd = ["yarn", "install"]
            pwa_returncode = run_process(pwa_cmd,
                                         wait=True,
                                         log=f,
                                         cwd=self.get_destination())
        if pwa_returncode != 0:
            raise IOError("PWA build failed with error code: %s" %
                          (pwa_returncode, ))

    def __run_build_pwa(self):
        with open(self.DEPLOY_LOG_PATH, "a") as f:
            pwa_cmd = ["yarn", "build", "--prod"]
            pwa_returncode = run_process(pwa_cmd,
                                         wait=True,
                                         log=f,
                                         cwd=self.get_destination())
        if pwa_returncode != 0:
            raise IOError("PWA build failed with error code: %s" %
                          (pwa_returncode, ))

    def __clean_source_code(self):
        dir_util.remove_tree(os.path.join(self.get_destination(), "e2e"))
        dir_util.remove_tree(os.path.join(self.get_destination(), "src"))
