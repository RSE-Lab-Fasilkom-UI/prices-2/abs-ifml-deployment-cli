import os
import shutil
from utils.administration import get_product_root


class FrontendInstance():
    def __init__(self, payload, config):
        self.payload = payload
        self.config = config
        self.PRODUCT_ROOT = get_product_root(config, payload["product_name"])
        self.DEPLOY_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                            config["logs::frontend_deploy"])
        self.RUN_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                         config["logs::frontend_run"])
        self.CURRENT_INSTANCE_PATH = os.path.join(self.PRODUCT_ROOT,
                                                  "frontend", "")
        self.UPDATE_TEMP_PATH = os.path.join(self.PRODUCT_ROOT,
                                             "frontend_update", "")
        self.OLD_INSTANCE_PATH = os.path.join(self.PRODUCT_ROOT,
                                              "frontend_old", "")
        if self.config.get("work_paths::node_location"):
            self.NODE_SHARED_PATH = os.path.abspath(
                os.path.expanduser(config["work_paths::node_location"]))
        else:
            self.NODE_SHARED_PATH = None
        self.__destination = self.CURRENT_INSTANCE_PATH

    def set_destination(self, destination):
        self.__destination = destination

    def get_destination(self):
        return self.__destination

    def build(self, preserve_source=False):
        raise NotImplementedError

    def run(self, socket):
        raise NotImplementedError

    def stop(self):
        raise NotImplementedError

    def update(self, preserve_source=False):
        raise NotImplementedError

    def revert(self):
        raise NotImplementedError

    def destroy(self):
        shutil.rmtree(self.get_destination(), ignore_errors=True)
