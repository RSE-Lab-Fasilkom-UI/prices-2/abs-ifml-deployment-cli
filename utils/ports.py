import socket
import time


def assign_ports(process_names=None):
    sockets = dict()
    for name in process_names:
        sockets[name] = reserve_port(0)
    return sockets


def reserve_ports(ports=None, exclude=None):
    sockets = dict()
    for name in ports:
        if name not in exclude:
            while not sockets.get(name):
                try:
                    sockets[name] = reserve_port(ports[name])
                except OSError:
                    print("Port for %s: %d already in use. Retrying..." %
                          (name, ports[name]))
                    time.sleep(1)
    return sockets


def extract_ports(process_sockets):
    ports = dict()
    for name in process_sockets:
        #Check apakah portnya manual atau otomatis pakai class socket
        if isinstance(process_sockets[name], int):
            ports[name] = process_sockets[name]
        else:
            ports[name] = process_sockets[name].getsockname()[1]
    return ports


def release_port(sock):
    if sock:
        sock.close()


def reserve_port(port):
    current_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    current_sock.bind(("", port))
    return current_sock
