import json
import os
import hashlib
import random
import re
import time
import shutil
from functools import reduce
from utils.db import query_to_database
from utils.ports import extract_ports
from utils.template import fill_template

APP_ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def check_product_exists(args, config):
    PRODUCT_ROOT = get_product_root(config, normalize_name(args.product_name))
    return os.path.exists(PRODUCT_ROOT)


def build_user_data(args):
    try:
        user_data_path = os.path.abspath(
            os.path.expanduser(args.user_data_file))
        user_file = open(user_data_path, "r")
        user_json = user_file.read()
        user_file.close()
        return json.loads(user_json)
    except (IOError, OSError, json.JSONDecodeError):
        return {}


def build_payload(args, config, sockets):
    payload = dict()
    payload["generator_codename"] = config.get("general::generator_codename",
                                               "splelive")
    payload["generator_home_address"] = config.get(
        "general::generator_home_address", "www.splelive.id")
    payload["google_analytics_id"] = config.get("general::google_analytics_id",
                                                "UA-XXXX-Y")

    payload["product_name"], payload["features"] = read_abs_products_file(
        args.product_name, args.products_file)
    payload["original_product_name"] = args.product_name
    payload["frontend_engine"] = args.frontend_engine
    payload["backend_engine"] = args.backend_engine
    payload["maintenance_mode"] = bool(args.maintenance_mode)

    payload["roles"] = __get_product_roles(args)
    payload["auth"] = __get_oauth2_params(args)

    payload["ports"] = extract_ports(sockets)
    payload["enable_ssl"] = args.enable_ssl
    payload["domain_name"] = config.get("general::domain_name", "localhost")

    payload["product_subdomain"] = __get_product_subdomain(args)
    payload["product_address"] = fill_template(os.path.join(
        APP_ROOT_PATH, "templates", "product_address.template"),
                                               payload,
                                               write_to_file=False)
    payload["full_product_address"] = fill_template(os.path.join(
        APP_ROOT_PATH, "templates", "full_product_address.template"),
                                                    payload,
                                                    write_to_file=False)
    payload["db_username"] = payload["product_subdomain"]
    payload["db_password"] = randomize_password()
    payload["docker_deploy"] = args.docker_deploy
    payload["ip_server_runner"] = config.get("general::ip_server_runner", "localhost")
    return payload


def read_json_payload(product_name, config):
    destination = get_product_root(config, normalize_name(product_name))
    json_path = os.path.join(destination,
                             normalize_name(product_name) + ".json")

    with open(json_path, "r") as f:
        return json.loads(f.read())


def write_json_payload(payload, config):
    PRODUCT_ROOT = get_product_root(config, payload["product_name"], makedir=True)
    json_path = os.path.join(PRODUCT_ROOT, payload["product_name"] + ".json")
    with open(json_path, "w" if os.path.exists(json_path) else "a") as f:
        f.write(json.dumps(payload, indent=4))


def read_abs_products_file(product_name, products_file_path):
    normalized_path = os.path.abspath(os.path.expanduser(products_file_path))
    print(normalized_path)
    normalized_name = normalize_name(product_name)
    with open(normalized_path, "r") as f:
        lines = [line.strip() for line in f.readlines()]
    for line in lines:
        if normalized_name in line:
            return normalized_name, __extract_product_features(line)

    print("Name not found in Products.abs, using first product defined in it.")
    print("Don't worry, the name that you've inputted will be preserved.")
    return normalized_name, __extract_product_features(lines[0])


def write_abs_products_file(payload, config):
    variables = dict(payload)
    PRODUCT_ROOT = get_product_root(config, payload["product_name"], makedir=True)
    products_abs_template_path = os.path.join(APP_ROOT_PATH, "templates", "Products.template.abs")
    products_abs_path = os.path.join(PRODUCT_ROOT, "Products.abs")
    shutil.copy(products_abs_template_path, products_abs_path)
    fill_template(products_abs_path, variables, write_to_file=True)


def normalize_name(product_name):
    word = []
    for x in product_name.split(" "):
        word.append(x[0].upper() + x[1:])
    return re.sub(r'[^\w]', '', " ".join(word))

def normalize_name_dash(product_name):
    return re.sub(r'[^\w-]', '', "-".join(product_name.split(" "))).lower()

def randomize_password(length=12):
    try:
        randomizer = random.SystemRandom()
    except NotImplementedError:
        randomizer = random
        print('A secure pseudo-random number generator is not available '
              'on your system. Falling back to Mersenne Twister.')
        state = randomizer.getstate()
        seed = (int(str(reduce((lambda x, y: x * y), state[1])).rstrip('0')) +
                sum(state[1]))**state[0]
        randomizer.seed(
            hashlib.sha256(('%s%s' % (seed, time.time())).encode()).digest())

    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    return ''.join(randomizer.choice(chars) for i in range(length))


def register_product(payload, user_data, config):
    variables = dict(payload)
    variables["user"] = user_data
    variables["database"] = config.get("database", "splelive_administration")
    query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                       "register_product.template.sql"),
                          variables,
                          write_to_file=False)
    return query_to_database(query, config)


def unregister_product(payload, config):
    variables = dict(payload)
    variables["database"] = config.get("database", "splelive_administration")
    query = fill_template(os.path.join(APP_ROOT_PATH, "templates",
                                       "unregister_product.template.sql"),
                          variables,
                          write_to_file=False)
    return query_to_database(query, config)


def get_product_root(config, product_name, makedir=False):
    DEPLOY_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::deploy_location"]))
    PRODUCT_ROOT = os.path.join(DEPLOY_ROOT, product_name)
    if makedir and not os.path.exists(PRODUCT_ROOT):
        os.makedirs(PRODUCT_ROOT, exist_ok=True)
    return PRODUCT_ROOT


def __extract_product_features(line):
    print(line)
    words = line.split(" ")
    return [
        x.strip() for x in " ".join(words[2:]).replace("(", "").replace(
            ");", "").split(",")
    ]


def __get_product_roles(args):
    if args.roles_file_path:
        with open(args.roles_file_path, "r") as f:
            return json.loads(f.read().strip())
    return json.loads(args.roles)


def __get_product_subdomain(args):
    if args.product_subdomain:
        return args.product_subdomain
    return normalize_name(args.product_name.lower())


def __get_oauth2_params(args):
    AVAILABLE_TYPES = ["auth0", "google", "facebook"]
    oauth2_payload = {}
    oauth2_payload["type"] = args.oauth2_type.lower()
    if args.oauth2_type in AVAILABLE_TYPES:
        oauth2_payload["client_id"] = args.oauth2_client_id
        oauth2_payload["client_secret"] = args.oauth2_client_secret
    return oauth2_payload
