import os
import shutil
import signal
import sys
import time
import argparse
from subprocess import Popen, call
from utils.administration import get_product_root

PLATFORM = sys.platform


def get_script_extension():
    if PLATFORM == "win32":
        return ".bat"
    return ".sh"


def get_python_exec():
    if PLATFORM == "win32":
        return "python"
    return "python3"


def get_log_redirection(path, stderr_only=False):
    log_path = os.path.abspath(path)
    if not stderr_only:
        return ['>>"%s"' % (log_path, ), "2>&1"]
    if PLATFORM == "win32":
        return [">NUL", '2>>"%s"' % (log_path, )]
    return [">/dev/null", '2>>"%s"' % (log_path, )]


def read_pid_file(dir_path):
    path = os.path.join(dir_path, "instance.pid")
    try:
        with open(path, "r") as f:
            return int(f.read().strip())
    except (IOError, ValueError):
        return None


def write_pid_to_file(proc, dir_path):
    path = os.path.join(dir_path, "instance.pid")
    if proc:
        pid = proc.pid
        with open(path, "w" if os.path.exists(path) else "a") as f:
            f.write(str(pid))
        return pid
    try:
        os.remove(path)
    except IOError:
        pass


def run_process(command_line, wait=False, shell=False, log=None, cwd=None):
    if PLATFORM == "win32":
        from subprocess import CREATE_NEW_PROCESS_GROUP
        cflag = CREATE_NEW_PROCESS_GROUP
        if wait:
            return call(command_line,
                        shell=shell,
                        creationflags=cflag,
                        stdout=log,
                        stderr=log,
                        cwd=cwd)
        return Popen(command_line,
                     shell=shell,
                     creationflags=cflag,
                     stdout=log,
                     stderr=log,
                     cwd=cwd)
    pfn = os.setsid
    if wait:
        return call(command_line,
                    shell=shell,
                    preexec_fn=pfn,
                    stdout=log,
                    stderr=log,
                    cwd=cwd)
    return Popen(command_line,
                 shell=shell,
                 preexec_fn=pfn,
                 stdout=log,
                 stderr=log,
                 cwd=cwd)


def stop_process(pid, sig=signal.SIGTERM):
    try:
        if PLATFORM != "win32":
            pgid = os.getpgid(pid)
            os.killpg(pgid, sig)
        else:
            print(pid)
            os.kill(pid, sig)
    except (OSError, ProcessLookupError):
        print("Instance process not found or missing! Skipping...")


def backup_logs(payload, config):
    PRODUCT_ROOT = get_product_root(config, payload["product_name"])
    LOG_BACKUP_ROOT = os.path.abspath(
        os.path.expanduser(config["work_paths::log_backup_location"]))
    destination = os.path.join(
        LOG_BACKUP_ROOT, "%s_%d" % (payload["product_name"], int(time.time())))
    if not os.path.exists(destination):
        os.makedirs(destination)

    for log_key in config["logs"]:
        log_path = os.path.join(PRODUCT_ROOT, config["logs"][log_key])
        base_name = os.path.basename(log_path)
        if os.path.exists(log_path):
            shutil.copy(log_path, os.path.join(destination, base_name))

def str_bool(bool_arg):
    if isinstance(bool_arg, bool):
        return bool_arg
    if bool_arg.lower() in ('true', 't'):
        return True
    elif bool_arg.lower() in ('false', 'f'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
