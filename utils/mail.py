import httplib2
import socks
import smtplib
import ssl
import traceback
from utils.template import fill_template

proxy = httplib2.proxy_info_from_environment()
if proxy:
    socks.setdefaultproxy(proxy.proxy_type, proxy.proxy_host, proxy.proxy_port)
    socks.wrapmodule(smtplib)

def send_email(payload, config, template_path, receiver, bcc: list = None):
    try:
        server = None
        mail_payload = dict(payload)
        mail_payload.update({
            "generator_name": config["general::generator_name"],
            "contact_person": config["email::sender_email"],
            "user": receiver
        })
        bcc = [
            x.strip() for x in config.get("email::admin_emails", "").split(",")
            if x
        ]

        message = fill_template(template_path,
                                mail_payload,
                                write_to_file=False)

        smtp_server = config["email::server"]
        port = int(config["email::port"])
        sender_email = config["email::sender_email"]
        sender_password = config["email::sender_password"]

        if config["email::type"].lower() == "tls":
            context = ssl.create_default_context()
            server = smtplib.SMTP(smtp_server, port)
            server.ehlo()
            server.starttls(context=context)
        elif config["email::type"].lower() == "ssl":
            context = ssl.create_default_context()
            server = smtplib.SMTP_SSL(smtp_server, port, context=context)
        else:
            server = smtplib.SMTP(smtp_server, port)

        server.ehlo()
        server.login(sender_email, sender_password)
        server.ehlo()

        to = [receiver["email"]]
        if bcc:
            to += bcc
        server.sendmail(sender_email, to, message)
    except Exception:
        traceback.print_exc()
        print("Failed to send email to %s using template from: %s." %
              (receiver["email"], template_path))
    finally:
        if server:
            server.quit()

def __connect_through_proxy():
    headers = """GET http://www.example.org HTTP/1.1
                Host: www.example.org\r\n\r\n"""

    host = "192.97.215.348" #proxy server IP
    port = 8080              #proxy server port

    try:
        s = socket.socket()
        s.connect((host,port))
        s.send(headers.encode('utf-8'))
        response = s.recv(3000)
        print (response)
        s.close()
    except socket.error as m:
       print (str(m))
       s.close()
       sys.exit(1)
