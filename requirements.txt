# CLI
httplib2
Jinja2
PySocks

# IFML generator
coloredlogs
beautifulsoup4
click
EditorConfig
jsbeautifier
MarkupSafe
pytz
six
yattag

# Admin API
Flask
Flask_Cors
gevent
python-daemon
python_interface

# Database
mysql-connector-python
psycopg2-binary==2.7.7
