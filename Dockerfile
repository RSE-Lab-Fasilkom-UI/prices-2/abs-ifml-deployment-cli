FROM nginx:alpine

WORKDIR /opt/{{subdomain_name}}
RUN mkdir -p /opt/{{subdomain_name}}

COPY {{roles_file_path}} roles.json
COPY {{products_abs_path}} abs/

