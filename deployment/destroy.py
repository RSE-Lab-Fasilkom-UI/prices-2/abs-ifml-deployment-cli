import os
import shutil
from deployment import initiate_instance_objects
from deployment.stop import stop_instances
from utils.administration import (check_product_exists, read_json_payload,
                                  unregister_product, get_product_root)
from utils.shell import backup_logs
from docker_deployment.docker_deploy import docker_destroy

def run_command(args, config):
    docker_build = args.docker_deploy

    if docker_build:
        result = docker_destroy(args.product_name)
        return result.json()['status']
    else:
        if not check_product_exists(args, config):
            raise FileNotFoundError(
                "Project %s not found. Please check deployment path in your config.ini!"
                % (args.product_name, ))
                
    payload = read_json_payload(args.product_name, config)
    instances = initiate_instance_objects(payload, config)

    stop_instances(instances)
    backup_logs(payload, config)
    __remove_product(payload, config, instances)
    unregister_product(payload, config)


def __remove_product(payload, config, instances):
    PRODUCT_ROOT = get_product_root(config, payload["product_name"])

    instances["admin"].destroy()
    instances["backend"].destroy()
    instances["frontend"].destroy()
    instances["nginx"].destroy()
    shutil.rmtree(PRODUCT_ROOT)
