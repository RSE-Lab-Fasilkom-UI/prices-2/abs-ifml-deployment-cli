from components.backend_vmj import BackendVMJInstance
from components.backend_erlang import BackendErlangInstance
from components.backend_java import BackendJavaInstance
from components.frontend_react import FrontendReactInstance
from components.frontend_angular import FrontendAngularInstance
from components.admin import AdminInstance
from components.backend import BackendInstance
from components.frontend import FrontendInstance
from components.nginx import NginxInstance


def initiate_instance_objects(payload, config):
    instances = {
        "admin": AdminInstance(payload, config),
        "nginx": NginxInstance(payload, config)
    }

    if payload["frontend_engine"].lower() == "react":
        instances["frontend"] = FrontendReactInstance(payload, config)
    elif payload["frontend_engine"].lower() == "angular":
        instances["frontend"] = FrontendAngularInstance(payload, config)

    if payload["backend_engine"].lower() == "absjava":
        instances["backend"] = BackendJavaInstance(payload, config)
    elif payload["backend_engine"].lower() == "abserlang":
        instances["backend"] = BackendErlangInstance(payload, config)
    elif payload["backend_engine"].lower() == "vmj":
        instances["backend"] = BackendVMJInstance(payload, config)

    return instances
