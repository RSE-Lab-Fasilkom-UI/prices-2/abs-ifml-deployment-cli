from components.nginx import NginxInstance
from utils.administration import (check_product_exists, read_json_payload,
                                  write_json_payload)
from docker_deployment.docker_deploy import docker_refresh_nginx


def run_command(args, config):
    docker_build = args.docker_deploy

    if docker_build:
        result = docker_refresh_nginx(args.product_name, args.maintenance_mode)
        return result.json()['status']
    else:
        if not check_product_exists(args, config):
            raise FileNotFoundError(
                "Project %s not found. Please check deployment path in your config.ini!"
                % (args.product_name, ))

        payload = read_json_payload(args.product_name, config)
        payload = __mutate_payload(payload, args, config)
        nginx = NginxInstance(payload, config)
        nginx.build()
        nginx.run(None)


def __mutate_payload(payload, args, config):
    new_payload = dict(payload)
    new_payload["maintenance_mode"] = bool(args.maintenance_mode)
    write_json_payload(new_payload, config)
    return new_payload
