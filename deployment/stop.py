from deployment import initiate_instance_objects
from utils.administration import check_product_exists, read_json_payload
from docker_deployment.docker_deploy import docker_stop

def run_command(args, config):
    docker_build = args.docker_deploy

    if docker_build:
        result = docker_stop(args.product_name)
        return result.json()['status']
    else :
        if not check_product_exists(args, config):
            raise FileNotFoundError(
                "Project %s not found. Please check deployment path in your config.ini!"
                % (args.product_name, ))
        
        payload = read_json_payload(args.product_name, config)
        instances = initiate_instance_objects(payload, config)
        stop_instances(instances)


def stop_instances(instances):
    for key in instances:
        instances[key].stop()
