import os
import shutil
from multiprocessing.pool import ThreadPool
from deployment import initiate_instance_objects
from deployment.destroy import run_command as destroy_product
from deployment.start import start_non_nginx_instances
from utils.administration import (APP_ROOT_PATH, build_payload,
                                  build_user_data, check_product_exists,
                                  write_json_payload, register_product)
from utils.mail import send_email
from utils.ports import assign_ports, release_port
from docker_deployment.docker_deploy import deploy_image, setup_deploy_server, get_deploy_port


def run_command(args, config):
    if check_product_exists(args, config):
        raise ValueError("Project %s already exists!" % (args.product_name, ))

    docker_build = args.docker_deploy

    if docker_build:
        sockets = dict()
        sockets["nginx"] = int(get_deploy_port())
        sockets["frontend"] = 6000
        sockets["backend"] = 8080
        sockets["admin"] = 8000

        payload = build_payload(args, config, sockets)
        write_json_payload(payload, config)
        user = build_user_data(args)
        instances = initiate_instance_objects(payload, config)

        send_email(
            payload, config,
            os.path.join(APP_ROOT_PATH, "templates",
                        "progress_email.template.txt"), user)

        nginx_build_success = instances["nginx"].build()
        build_success = nginx_build_success and build_instances(
            instances, preserve_source=args.preserve_source)
        if not build_success:
            return __handle_failed_builds(args, payload, config, user)

        deploy_root = os.path.abspath(
            os.path.expanduser(config["work_paths::deploy_location"]))
        product_path = os.path.join(deploy_root,
                                         payload["product_name"])
        backend_product_path = os.path.join(product_path, "backend")
        nginx_product_path = os.path.join(product_path, "nginx")

        register_product(payload, user, config)

        setup_deploy_server(payload, args.product_name.replace(" ",""), backend_product_path, nginx_product_path)
                                         
        product_name = args.product_name.replace(" ","").lower()

        #copy dockerfile to deploy_location
        shutil.copy("docker_deployment/Dockerfile", product_path)
        #copy .dockerignore to deploy_location
        shutil.copy("docker_deployment/.dockerignore", product_path)
        #copy nginx.conf to deploy_location
        shutil.copy("docker_deployment/nginx.conf", product_path)
        #copy start.sh to deploy_location
        shutil.copy("docker_deployment/start.sh", product_path)
        #copy product nginx.conf
        nginx_product_conf_path = "_nginx/" + args.product_name.replace(" ","") + ".conf"
        nginx_product_conf_dest = os.path.join(os.path.abspath(product_path), "_nginx/")
        os.makedirs(nginx_product_conf_dest)
        shutil.copyfile(nginx_product_conf_path, nginx_product_conf_dest + product_name + ".conf")

        result = deploy_image(product_name, product_path, sockets['nginx'], payload['frontend_engine'], payload['maintenance_mode'])
        #copy docker.log to deploy_location
        shutil.copy("docker_deployment/" + product_name + ".log", os.path.join(product_path, config["logs::docker_deploy"]))
        os.remove("docker_deployment/" + product_name+ ".log")

        send_email(
            payload, config,
            os.path.join(APP_ROOT_PATH, "templates",
                        "finished_email.template.txt"), user)

        if result:
            print("Docker Deployment produk " + args.product_name + " berhasil !")
        else:
            print("Docker Deployment produk " + args.product_name + " gagal !")

    else:
        sockets = assign_ports(["frontend", "backend", "admin", "nginx"])

        payload = build_payload(args, config, sockets)
        write_json_payload(payload, config)
        user = build_user_data(args)
        instances = initiate_instance_objects(payload, config)

        send_email(
            payload, config,
            os.path.join(APP_ROOT_PATH, "templates",
                        "progress_email.template.txt"), user)

        nginx_build_success = instances["nginx"].build()
        if nginx_build_success:
            instances["nginx"].run(sockets["nginx"])

        build_success = nginx_build_success and build_instances(
            instances, preserve_source=args.preserve_source)
        if not build_success:
            return __handle_failed_builds(args, payload, config, user)

        register_product(payload, user, config)
        start_non_nginx_instances(instances, sockets)
        send_email(
            payload, config,
            os.path.join(APP_ROOT_PATH, "templates",
                        "finished_email.template.txt"), user)


def build_instances(instances, preserve_source=False):
    thread_pool = ThreadPool(processes=3)
    build_threads = {
        "backend":
        thread_pool.apply_async(instances["backend"].build,
                                kwds={"preserve_source": preserve_source}),
        "frontend":
        thread_pool.apply_async(instances["frontend"].build,
                                kwds={"preserve_source": preserve_source}),
        "admin":
        thread_pool.apply_async(instances["admin"].build)
    }

    failed = []
    for key in build_threads:
        build_success = build_threads[key].get()
        if not build_success:
            failed.append(key)
            print("Build failed on instances: %s" % (key, ))
    return not failed


def __handle_failed_builds(args, payload, config, user):
    send_email(
        payload, config,
        os.path.join(APP_ROOT_PATH, "templates", "failed_email.template.txt"),
        user)
    return destroy_product(args, config)
